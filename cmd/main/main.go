package main

import (
	"burhanudinr.com/tugas1/internal/app/delivery"
	telegramService "burhanudinr.com/tugas1/internal/app/im/telegram/service"
	"github.com/gin-gonic/gin"
)

func main() {
	route := gin.Default()

	route.GET("/", func(c *gin.Context) {
		delivery.Success(c, nil, "endpoint alive", 0)
	})

	route.POST("/", telegramService.Callback)

	_ = route.Run(":8099")
}
