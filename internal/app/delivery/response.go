package delivery

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func Success(c *gin.Context, data interface{}, msg string, statusCode int) {
	if msg == "" {
		msg = "Success"
	}
	if statusCode == 0 {
		statusCode = http.StatusOK
	}

	c.JSON(statusCode, gin.H{
		"data":    data,
		"message": msg,
	})
}

func Failed(c *gin.Context, err error, statusCode int) {

	if statusCode == 0 {
		statusCode = http.StatusInternalServerError
	}

	c.AbortWithStatusJSON(statusCode, gin.H{
		"message": err.Error(),
	})
}
