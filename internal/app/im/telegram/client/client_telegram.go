package client

import (
	"burhanudinr.com/tugas1/internal/app/utils"
	"encoding/json"
	"github.com/go-resty/resty/v2"
)

var url = "https://api.telegram.org/bot1622543955:AAEZDHQc9Z02j4KmYmvayhwDk0DeD3vo_Wo"

func SendMessage(request interface{}) (interface{}, error) {
	utils.Pretty(request)
	res, err := resty.New().R().SetBody(request).Post(url + "/sendMessage")
	if err != nil {
		return nil, err
	}

	jsonRes, _ := json.Marshal(res.Body())
	utils.Pretty(jsonRes)

	return jsonRes, nil
}

func SendPhoto(request interface{}) (interface{}, error) {
	utils.Pretty(request)
	res, err := resty.New().R().SetBody(request).Post(url + "/sendPhoto")
	if err != nil {
		return nil, err
	}

	jsonRes, _ := json.Marshal(res.Body())
	utils.Pretty(jsonRes)
	return jsonRes, nil
}
