package service

import (
	"burhanudinr.com/tugas1/internal/app/delivery"
	"burhanudinr.com/tugas1/internal/app/im/telegram/client"
	"burhanudinr.com/tugas1/internal/app/im/telegram/domain"
	"burhanudinr.com/tugas1/internal/app/utils"
	"github.com/gin-gonic/gin"
	"strings"
)

var err error
var result interface{}

func Callback(c *gin.Context) {
	var request domain.TelegramResponse
	if err := c.Bind(&request); err != nil {
		delivery.Failed(c, err, 0)
		return
	}

	utils.Pretty(request)

	if strings.Contains(request.Message.Text, "namaku adalah") {
		name := strings.Split(request.Message.Text, "adalah")
		result, err = client.SendMessage(domain.Reply{
			ChatID: request.Message.Chat.ID,
			Text:   "Halo selamat datang " + name[1],
		})

	} else {
		switch request.Message.Text {
		case "teks":
			result, err = client.SendMessage(domain.Reply{
				ChatID: request.Message.Chat.ID,
				Text:   "Halo guys!",
			})

			break
		case "halo", "hi", "hallo":
			result, err = client.SendMessage(domain.Reply{
				ChatID: request.Message.Chat.ID,
				Text:   "Siapa nama kamu ?",
			})
			break
		case "gambar":
			result, err = client.SendPhoto(domain.Reply{
				ChatID: request.Message.Chat.ID,
				Photo:  "http://indrapermana.net/wp-content/uploads/2018/07/Hallo.jpg",
			})

			break
		case "tombol":
			result, err = client.SendMessage(domain.Reply{
				ChatID: request.Message.Chat.ID,
				Text:   "Pilih Tombol dibawah ini",
				ReplyMarkup: &domain.Button{
					InlineKeyboard: [][]domain.InlineKeyboard{
						{
							{
								"puas",
								"callback puas",
								"",
							},
							{
								"tidak puas",
								"callback tidak puas",
								"",
							},
						},
					},
				},
			})

			break
		default:
			result, err = client.SendMessage(domain.Reply{
				ChatID: request.Message.Chat.ID,
				Text:   "message tidak dikenali",
			})
		}
	}

	if err != nil {
		delivery.Failed(c, err, 0)
		return
	}

	delivery.Success(c, result, "success", 0)
}
