package domain

type TelegramResponse struct {
	Message struct {
		Chat struct {
			FirstName string `json:"first_name"`
			ID        int    `json:"id"`
			Type      string `json:"type"`
			Username  string `json:"username"`
		} `json:"chat"`
		Date int `json:"date"`
		From struct {
			FirstName    string `json:"first_name"`
			ID           int    `json:"id"`
			IsBot        bool   `json:"is_bot"`
			LanguageCode string `json:"language_code"`
			Username     string `json:"username"`
		} `json:"from"`
		MessageID int    `json:"message_id"`
		Text      string `json:"text"`
		Photo     []struct {
			FileID       string `json:"file_id"`
			FileSize     int    `json:"file_size"`
			FileUniqueID string `json:"file_unique_id"`
			Height       int    `json:"height"`
			Width        int    `json:"width"`
		} `json:"photo"`
	} `json:"message"`
	UpdateID int `json:"update_id"`
}

type Reply struct {
	ChatID      int     `json:"chat_id,omitempty"`
	ReplyMarkup *Button `json:"reply_markup,omitempty"`
	Text        string  `json:"text,omitempty"`
	ParseMode   string  `json:"parse_mode,omitempty"`
	Photo       string  `json:"photo,omitempty"`
	Document    string  `json:"document,omitempty"`
	URL         string  `json:"url,omitempty"`
	Caption     string  `json:"caption,omitempty"`
}

type Button struct {
	InlineKeyboard [][]InlineKeyboard `json:"inline_keyboard,omitempty"`
}

type InlineKeyboard struct {
	Text         string `json:"text,omitempty"`
	CallbackData string `json:"callback_data,omitempty"`
	URL          string `json:"url,omitempty"`
}
