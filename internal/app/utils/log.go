package utils

import (
	"encoding/json"
	"fmt"
)

func Pretty(data interface{}) {
	b, err := json.MarshalIndent(data, "", "  ")
	if err != nil {
		fmt.Println("error:", err)
	}
	fmt.Print(string(b) + "\n")
}
